//Package principal
package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
)

//Structure qui représente une personne obtenue avec getPersonneListe
type Personne struct {
	n_personne              int    `json:"id"`
	nom_personne            string `json:"nom"`
	prenom_personne         string `json:"prenom"`
	date_naissance_personne string `json:"date_de_naissance"`
	tel_personne            string `json:"numero_telephone"`
}

//Structure qui représente un médecin et son patient
type MedecinPatient struct {
	medecin Personne `json:"medecin"`
	patient Personne `json:"patient"`
}

//Structure qui rerésente une maladie obtenue avec getMaladie
type Maladie struct {
	n_maladie_patient        int    `json:"id"`
	nom_maladie_patient      string `json:"nom_maladie"`
	date_declaration_maladie string `json: "date_declariation"`
}

//Stucture qui représente un médicament obtenue avec getMedicament
type Medicament struct {
	n_medicament   int    `json:"id"`
	nom_medicament string `json:"nom_medicament"`
	duree_jour     int    `json:"duree"`
	nbr_fois       int    `json:"frequence"`
}

var db *sql.DB

//Requete SQL pour récupérer les patients d'un médecin
func getPersonneListe(id string) ([]Personne, error) {
	var lesPer []Personne
	//Requete SQL
	res, err := db.Query("SELECT A.* FROM med_personne A JOIN med_patient B on n_personne = n_patient JOIN med_traitant C on B.n_patient = C.n_patient WHERE n_medecin = ?", id)
	if err != nil {
		return nil, err
	}
	defer res.Close()
	//Pour tout les éléments obtenue, si il n'y a pas d'erreur on ajoute la ligne au tableau de valeur a retourner
	for res.Next() {
		var per Personne
		err := res.Scan(&per.n_personne, &per.nom_personne, &per.prenom_personne, &per.date_naissance_personne, &per.tel_personne)
		if err != nil {
			return lesPer, err
		}
		lesPer = append(lesPer, per)
	}
	return lesPer, nil
}

//Affichage des patients d'un docteur
func getPersonne(c *gin.Context) {
	//On recupère les valeurs
	id := c.Param("medecin")
	lesPer, err := getPersonneListe(id)
	if err != nil {
		log.Fatal(err)
	}
	//On affiche
	for _, per := range lesPer {
		fmt.Printf("Personne found: %v\n", per)
		message := "id :" + strconv.Itoa(per.n_personne) + ", nom : " + per.nom_personne + ", prenom : " + per.prenom_personne + ", date de naissance : " + per.date_naissance_personne + ", téléphone : " + per.tel_personne + "\n"
		c.String(http.StatusOK, message)
		getHistoriqueMaladie(c, per.n_personne)
	}
}

// Requete SQL pour récupérer les maladies d'un patient
func getMaladie(id int) ([]Maladie, error) {
	var lesMal []Maladie
	//Requete SQL
	res, err := db.Query("SELECT n_maladie_personne,nom_maladie_patient,date_declaration_maladie FROM med_historique_maladie WHERE n_patient = ?", id)
	if err != nil {
		return nil, err
	}
	defer res.Close()
	//Pour tout les éléments obtenue, si il n'y a pas d'erreur on ajoute la ligne au tableau de valeur a retourner
	for res.Next() {
		var mal Maladie
		err := res.Scan(&mal.n_maladie_patient, &mal.nom_maladie_patient, &mal.date_declaration_maladie)
		if err != nil {
			return lesMal, err
		}
		lesMal = append(lesMal, mal)
	}
	return lesMal, nil
}

// Affichage des maladies d'un patient
func getHistoriqueMaladie(c *gin.Context, id int) {
	//On récupère les valeurs
	lesMal, err := getMaladie(id)
	if err != nil {
		log.Fatal(err)
	}
	//On affiche
	for _, mal := range lesMal {
		fmt.Printf("maladie found: %v\n", mal)
		message := "\t id : " + strconv.Itoa(mal.n_maladie_patient) + ", maladie : " + mal.nom_maladie_patient + ", date :" + mal.date_declaration_maladie + "\n"
		c.String(http.StatusOK, message)
		getMedicamentList(c, mal.n_maladie_patient)
	}
}

//Requete SQL pour récupérer les médicaments prescrits pour la maladie d'un patient
func getMedicament(id int) ([]Medicament, error) {
	var lesMedi []Medicament
	//Requete SQL
	res, err := db.Query("SELECT n_medicament,nom_medicament,duree_jour,nbr_fois_jour FROM med_medicament JOIN med_prescription USING (n_prescription) WHERE n_maladie_patient = ?", id)
	if err != nil {
		return nil, err
	}
	defer res.Close()
	//Pour tout les éléments obtenue, si il n'y a pas d'erreur on ajoute la ligne au tableau de valeur a retourner
	for res.Next() {
		var medi Medicament
		err := res.Scan(&medi.n_medicament, &medi.nom_medicament, &medi.duree_jour, &medi.nbr_fois)
		if err != nil {
			return lesMedi, err
		}
		lesMedi = append(lesMedi, medi)
	}
	return lesMedi, nil
}

//Affichage des medicaments selon l'id d'une maladie d'un patient
func getMedicamentList(c *gin.Context, id int) {
	//Recupère les valeurs
	lesMedi, err := getMedicament(id)
	if err != nil {
		log.Fatal(err)
	}
	//On les affiches
	for _, medi := range lesMedi {
		fmt.Printf("Medicament found: %v\n", medi)
		message := "\t\t id : " + strconv.Itoa(medi.n_medicament) + ", medicament : " + medi.nom_medicament + ", duree (jour) : " + strconv.Itoa(medi.duree_jour) + ", Nombre de fois par jour : " + strconv.Itoa(medi.nbr_fois) + "\n"
		c.String(http.StatusOK, message)
	}
}

//Insertion d'une nouvelle personne dans la BDD
func postPatient(c *gin.Context) {
	var per Personne

	if err := c.BindJSON(&per); err != nil {
		return
	}

	// Insert dans la bdd
	sqlInsert := `
	INSERT INTO med_personne (n_personne,nom_personne,prenom_personne,date_naissance_personne,tel_personne) 
	values (?,?,?,?,?)`
	err := db.QueryRow(sqlInsert, per.n_personne, per.nom_personne, per.prenom_personne, per.date_naissance_personne, per.tel_personne)
	if err != nil {
		log.Fatal(err)
	}
	sqlInsert = `INSERT med_patient (n_patient) values (?)`
	err = db.QueryRow(sqlInsert, per.n_personne)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Success")
}

func postPatientMedecin(c *gin.Context) {
	var couple MedecinPatient

	if err := c.BindJSON(&couple); err != nil {
		return
	}
	medecin := couple.medecin
	patient := couple.patient

	// Insert dans la bdd
	sqlInsert := `
	INSERT INTO med_traitant (n_medecin,n_patient) 
	values (?,?)`
	err := db.QueryRow(sqlInsert, medecin.n_personne, patient.n_personne)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Success")
}

//Fonction main
func main() {
	//Connection a la base de donnée
	var err error
	db, err = sql.Open("mysql", "root:@tcp(127.0.0.1:3306)/medecin")
	if err != nil {
		log.Fatal(err)
	}
	pingErr := db.Ping()
	if pingErr != nil {
		log.Fatal(pingErr)
	}
	fmt.Println("Connected!")

	router := gin.Default()
	//Handler
	router.GET("/liste/:medecin", getPersonne)
	router.POST("/addPersonne", postPatient)
	router.POST("/addPatient", postPatientMedecin)

	router.Run("localhost:8080")
}
