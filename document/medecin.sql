-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : mar. 18 jan. 2022 à 22:04
-- Version du serveur : 10.4.19-MariaDB
-- Version de PHP : 8.0.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `medecin`
--

-- --------------------------------------------------------

--
-- Structure de la table `med_historique_maladie`
--

CREATE TABLE `med_historique_maladie` (
  `n_maladie_personne` int(3) NOT NULL,
  `n_patient` int(5) NOT NULL,
  `nom_maladie_patient` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `date_declaration_maladie` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Déchargement des données de la table `med_historique_maladie`
--

INSERT INTO `med_historique_maladie` (`n_maladie_personne`, `n_patient`, `nom_maladie_patient`, `date_declaration_maladie`) VALUES
(0, 0, 'Asthme', '2021-06-08'),
(1, 2, 'Angine', '2022-01-24'),
(2, 2, 'Fièvre', '2022-01-03');

-- --------------------------------------------------------

--
-- Structure de la table `med_medecin`
--

CREATE TABLE `med_medecin` (
  `n_medecin` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Déchargement des données de la table `med_medecin`
--

INSERT INTO `med_medecin` (`n_medecin`) VALUES
(1);

-- --------------------------------------------------------

--
-- Structure de la table `med_medicament`
--

CREATE TABLE `med_medicament` (
  `n_prescription` int(6) NOT NULL,
  `n_medicament` int(5) NOT NULL,
  `nom_medicament` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `duree_jour` int(3) NOT NULL,
  `nbr_fois_jour` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Déchargement des données de la table `med_medicament`
--

INSERT INTO `med_medicament` (`n_prescription`, `n_medicament`, `nom_medicament`, `duree_jour`, `nbr_fois_jour`) VALUES
(0, 0, 'Montélukast', 7, 2),
(1, 1, 'Paracétamol', 2, 3);

-- --------------------------------------------------------

--
-- Structure de la table `med_patient`
--

CREATE TABLE `med_patient` (
  `n_patient` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Déchargement des données de la table `med_patient`
--

INSERT INTO `med_patient` (`n_patient`) VALUES
(0),
(2),
(4);

-- --------------------------------------------------------

--
-- Structure de la table `med_personne`
--

CREATE TABLE `med_personne` (
  `n_personne` int(5) NOT NULL,
  `nom_personne` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `prenom_personne` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `date_naissance_personne` date NOT NULL,
  `tel_personne` varchar(10) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Déchargement des données de la table `med_personne`
--

INSERT INTO `med_personne` (`n_personne`, `nom_personne`, `prenom_personne`, `date_naissance_personne`, `tel_personne`) VALUES
(0, 'GAUTIER', 'Sylvain', '1998-12-12', '0707070707'),
(1, 'INGUE', 'Sera', '2001-09-08', '0606060606'),
(2, 'Cien', 'Pate', '1965-02-02', '0667766767'),
(4, 'Tropi', 'Patrick', '1985-06-05', '0785858585');

-- --------------------------------------------------------

--
-- Structure de la table `med_prescription`
--

CREATE TABLE `med_prescription` (
  `n_prescription` int(6) NOT NULL,
  `n_maladie_patient` int(3) NOT NULL,
  `n_medecin` int(5) NOT NULL,
  `date_prescription` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Déchargement des données de la table `med_prescription`
--

INSERT INTO `med_prescription` (`n_prescription`, `n_maladie_patient`, `n_medecin`, `date_prescription`) VALUES
(0, 0, 1, '2021-06-08'),
(1, 1, 1, '2022-01-25'),
(2, 2, 1, '2022-01-03');

-- --------------------------------------------------------

--
-- Structure de la table `med_traitant`
--

CREATE TABLE `med_traitant` (
  `n_medecin` int(5) NOT NULL,
  `n_patient` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Déchargement des données de la table `med_traitant`
--

INSERT INTO `med_traitant` (`n_medecin`, `n_patient`) VALUES
(1, 0),
(1, 2);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `med_historique_maladie`
--
ALTER TABLE `med_historique_maladie`
  ADD PRIMARY KEY (`n_maladie_personne`,`n_patient`),
  ADD KEY `FK_historique_patient` (`n_patient`);

--
-- Index pour la table `med_medecin`
--
ALTER TABLE `med_medecin`
  ADD PRIMARY KEY (`n_medecin`);

--
-- Index pour la table `med_medicament`
--
ALTER TABLE `med_medicament`
  ADD PRIMARY KEY (`n_prescription`,`n_medicament`);

--
-- Index pour la table `med_patient`
--
ALTER TABLE `med_patient`
  ADD PRIMARY KEY (`n_patient`);

--
-- Index pour la table `med_personne`
--
ALTER TABLE `med_personne`
  ADD PRIMARY KEY (`n_personne`);

--
-- Index pour la table `med_prescription`
--
ALTER TABLE `med_prescription`
  ADD PRIMARY KEY (`n_prescription`),
  ADD KEY `FK_PRESCRIPTION_MEDECIN` (`n_medecin`),
  ADD KEY `FK_PRESCRIPTION_HISTORIQUE` (`n_maladie_patient`);

--
-- Index pour la table `med_traitant`
--
ALTER TABLE `med_traitant`
  ADD PRIMARY KEY (`n_medecin`,`n_patient`),
  ADD KEY `FK_TRAINTANT_PATIENT` (`n_patient`);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `med_historique_maladie`
--
ALTER TABLE `med_historique_maladie`
  ADD CONSTRAINT `FK_historique_patient` FOREIGN KEY (`n_patient`) REFERENCES `med_patient` (`n_patient`);

--
-- Contraintes pour la table `med_medecin`
--
ALTER TABLE `med_medecin`
  ADD CONSTRAINT `FK_MEDECIN_PERSONNE` FOREIGN KEY (`n_medecin`) REFERENCES `med_personne` (`n_personne`);

--
-- Contraintes pour la table `med_medicament`
--
ALTER TABLE `med_medicament`
  ADD CONSTRAINT `FK_MEDICAMENT_PRESCRIPTION` FOREIGN KEY (`n_prescription`) REFERENCES `med_prescription` (`n_prescription`);

--
-- Contraintes pour la table `med_patient`
--
ALTER TABLE `med_patient`
  ADD CONSTRAINT `FK_PATIENT_PERSONNE` FOREIGN KEY (`n_patient`) REFERENCES `med_personne` (`n_personne`);

--
-- Contraintes pour la table `med_prescription`
--
ALTER TABLE `med_prescription`
  ADD CONSTRAINT `FK_PRESCRIPTION_HISTORIQUE` FOREIGN KEY (`n_maladie_patient`) REFERENCES `med_historique_maladie` (`n_maladie_personne`),
  ADD CONSTRAINT `FK_PRESCRIPTION_MEDECIN` FOREIGN KEY (`n_medecin`) REFERENCES `med_medecin` (`n_medecin`);

--
-- Contraintes pour la table `med_traitant`
--
ALTER TABLE `med_traitant`
  ADD CONSTRAINT `FK_TRAINTANT_MEDECIN` FOREIGN KEY (`n_medecin`) REFERENCES `med_medecin` (`n_medecin`),
  ADD CONSTRAINT `FK_TRAINTANT_PATIENT` FOREIGN KEY (`n_patient`) REFERENCES `med_patient` (`n_patient`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
