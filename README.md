EndPoint :  
- "/liste/:medecin" : affiche la liste des patients d'un médecin selon le paramètre passé.  
- "/addPersonne" : pour ajouter une personne dans la base de données.
- "/addPatient" : pour ajouter un patient à un médecin.

Ce que j'aurais aimé faire avec plus de temps : 
- Gager en aisance avec la syntaxe du go, et les normes pour faire un projet dans ce language.
- Corriger les erreurs signalées mais ne semblant pas empecher le fonctionnement du programme (ex : import gin-gonic et sql-driver).
- Améliorer la qualité du code (par exemple pas tout dans le même fichier).
- Achever/compléter les parties commencées, mais non terminées (documentation,MakeFile...).
- Vérifier le bon fonctionnement des POST. 
- Prendre plus connaissance des outils externes utile au développement d'une API comme Insomnia REST.
- Finaliser ce projet.

Comment j'envisageais la suite : 
- Supprimer une personne -> installer un handler DELETE et faire un delete dans la base de données.
- Modifier une personne -> installer un handler PUT et faire un update dans la base de données.
- Statistique -> Faire un serveur sur le port 8081, connecté à la base de données medecin. Qui fait des requêtes sur la base de donnée selon des statistiques souhaités (handler GET par exemple).
- Authentification -> Serveur sur le port 8082, connecté à la base de donnée auth_medecin. Qui aurait un POST comme page de connection, vérifie la validité des données saisies et redirige vers la page demandée. 
